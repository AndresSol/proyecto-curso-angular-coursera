import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { OpcionDestino } from '../../models/opcion-destino.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-states.model';
import { AppState } from '../../app.module';


@Component({
  selector: 'lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})

export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded:EventEmitter<OpcionDestino>;
  updates: string[];
  all: any;


  //destinos: OpcionDestino[];
  constructor( public destinosApiClient:DestinosApiClient, public store: Store<AppState> ) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    // this.destinosApiClient.subscribeOnChange((d: OpcionDestino) => {
    //   if (d != null) {
    //     this.updates.push('Se ha elegido a ' + d.nombre);
    //   }
    // });
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.updates.push('Se eligió: ' + d.nombre);
          // const fav = d;
        }
    });

    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }
  /*
  guardar(nombre:string, url:string):boolean {
    this.destinos.push(new OpcionDestino(nombre, url));
    //console.log(new OpcionDestino(nombre,url));
    //console.log(this.destinos);
    return false;
  }*/
  agregado(d: OpcionDestino) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    // this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(e: OpcionDestino){
    //desmarcar todos los demas en en array de elegidos
    //this.destinos.forEach(function (x) {x.setSelected(false); });
    //se marca el elegido
    //d.setSelected(true);
    this.destinosApiClient.elegir(e);
    // this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll(){

  }

}