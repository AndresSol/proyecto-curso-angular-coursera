import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpcionDestinoComponent } from './opcion-destino.component';

describe('OpcionDestinoComponent', () => {
  let component: OpcionDestinoComponent;
  let fixture: ComponentFixture<OpcionDestinoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpcionDestinoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpcionDestinoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
