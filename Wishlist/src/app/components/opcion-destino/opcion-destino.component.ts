import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { OpcionDestino } from './../../models/opcion-destino.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-states.model';
import { trigger, state, style, transition, animate } from '@angular/animations';


@Component({
  selector: 'opcion-destino',
  templateUrl: './opcion-destino.component.html',
  styleUrls: ['./opcion-destino.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class OpcionDestinoComponent implements OnInit {

  // Declaración de variables
  @Input() destino: OpcionDestino;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked: EventEmitter<OpcionDestino>;

  constructor( public store: Store<AppState> ) { 
  	this.destino = new OpcionDestino("",""); 
    this.onClicked = new EventEmitter();
    this.position = 1;
  }

  ngOnInit(): void {
  }

   ir() {
     // console.log(this.destino);
    this.onClicked.emit(this.destino);
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
