import { OpcionDestino } from '../models/opcion-destino.model';
import { Subject, BehaviorSubject, Observable, throwError, of } from 'rxjs';
import { Store } from '@ngrx/store';
// import { Http, Response } from '@angular/http';
import { catchError, map } from 'rxjs/operators';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-states.model';
import { AppState } from '../app.module';
import { Injectable, ElementRef, ViewChild, Inject, forwardRef } from '@angular/core';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import { DestinosViajesState } from '../models/destinos-viajes-states.model';
import { AppConfig, APP_CONFIG, MyDatabase, db } from 'src/app/app.module';


@Injectable()
export class DestinosApiClient {
  destinos:OpcionDestino[];
  // current: Subject<OpcionDestino> = new BehaviorSubject<OpcionDestino>(null);

  constructor(private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient) {
       this.destinos = [];
  }
  add(d: OpcionDestino) {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
    this.http.request(req).subscribe(data => {
      // if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d))
        // (error: any) => this.errorMessage = <any>error,
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then(destinos => console.log(destinos))
      // }
      // return this.http.get('home');
    });
  }
  // getAll(){
  //   return this.destinos;
  // }
  getById(id:string): OpcionDestino {
    return this.destinos.filter(function(d) {return d.id.toString() === id;})[0];
    // return this.store.select(id);
  }
  elegir(d: OpcionDestino){
    this.store.dispatch(new ElegidoFavoritoAction(d));
    // this.destinos.forEach(x=> x.setSelected(false));
    // d.setSelected(true);
    // this.current.next(d);
  }

  // subscribeOnChange( fn: any ){
  //   this.current.subscribe(fn);
  // }
}