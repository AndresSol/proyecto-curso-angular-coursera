import { DestinosApiClient } from './destinos-api-client.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { v4 as uuidv4 } from 'uuid';

// @Injectable({ providedIn: DestinosApiClient})

export class OpcionDestino {
 	selected: boolean;
 	id = uuidv4();

	constructor(public nombre:string, public imagenUrl:string, public votes: number = 0){
		// this.selected = false;
	}

	isSelected() {
		return this.selected;
	}
	
	setSelected(s: boolean) {
		console.log(this.selected);
    	this.selected = s;
  	}

  	VoteUp(){
  		this.votes++;
  	}

  	VoteDown(){
  		this.votes--;
  	}
}